package tpi2_gym;

public class Treinador extends Assalariado {

    public Treinador(String Nome, int idd, String genero, float salario) {
        super(Nome, idd, genero, salario);
    }

    @Override
    public float CalculaSalario() {
        return this.getSalario();
    }

    @Override
    public String toString() {
        return super.toString() + "\nSalario= " + CalculaSalario() + "€";
    }

}
