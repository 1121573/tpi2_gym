package tpi2_gym;

public abstract class Pessoa {

    private int idd;
    private String Nome;
    private String genero;

    public Pessoa(String Nome, int idd, String genero) {
        this.Nome = Nome;
        this.idd = idd;
        this.genero = genero;
    }

    public Pessoa() {
        this("Sem Nome", 0, "N/D");
    }

    public String getNome() {
        return Nome;
    }

    public String getGenero() {
        return genero;
    }

    public int getIdd() {
        return idd;
    }

    public void setGenero(String val) {
        this.genero = val;
    }

    public void setNome(String val) {
        this.Nome = val;
    }

    public void setIdd(int val) {
        this.idd = val;
    }

    @Override
    public String toString() {
        return "\nNome=" + Nome
                + "\nIdade=" + idd
                + "\nGenero=" + genero;
    }

}
