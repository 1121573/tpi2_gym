package tpi2_gym;

public abstract class Clientes extends Pessoa {

    //variaveis de instância
    private float peso;
    private float altura;
    private boolean activo;

    //variaveis de classes
    private static int MIN_IMC = 18;
    private static int MAX_IMC = 25;

    //construtores
    public Clientes(String Nome, int idd, String genero, float peso, float altura, boolean activo) {
        super(Nome, idd, genero);
        this.peso = peso;
        this.altura = altura;
        this.activo = activo;
    }

    public Clientes() {
        super();
        this.peso = 0;
        this.altura = 0;
        this.activo = false;
    }

    public Clientes(Clientes duplicado) {
        this(duplicado.getNome(), duplicado.getIdd(), duplicado.getGenero(),
                duplicado.getPeso(), duplicado.getAltura(), duplicado.isActivo());
    }

    //metodos de instancia
    public float getAltura() {
        return altura;
    }

    public void setAltura(float val) {
        this.altura = val;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float val) {
        this.peso = val;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public float CalculaIMC() {
        return (float) ((peso) / (Math.pow(altura, 2)));
    }

    public String grauObesidadeToString() {
        String ob;

        if (CalculaIMC() < MIN_IMC) {
            ob = "Magro";
        } else if (CalculaIMC() > MAX_IMC) {
            ob = "Obeso";
        } else {
            ob = "Normal";
        }

        return ob;

    }

    @Override
    public String toString() {

        return super.toString() + "\npeso=" + peso
                + "\naltura=" + altura
                + "\nEstado activo?=" + activo
                + String.format("\nIMC=%.2f", CalculaIMC())
                + "\nGrau Obesidade=" + grauObesidadeToString();
    }

    //metodos de class
    public static int getMAX_IMC() {
        return MAX_IMC;
    }

    public static void setMAX_IMC(int val) {
        MAX_IMC = val;
    }

    public static int getMIN_IMC() {
        return MIN_IMC;
    }

    public static void setMIN_IMC(int val) {
        MIN_IMC = val;
    }

}
