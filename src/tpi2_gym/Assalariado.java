package tpi2_gym;

public abstract class Assalariado extends Pessoa {

    private float salario;

    public Assalariado(String Nome, int idd, String genero, float salario) {
        super(Nome, idd, genero);
        this.salario = salario;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float val) {
        this.salario = val;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public abstract float CalculaSalario();

}
