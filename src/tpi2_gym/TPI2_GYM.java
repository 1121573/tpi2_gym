package tpi2_gym;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Brito
 */
public class TPI2_GYM {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        ClienteRegular cr1 = new ClienteRegular("Manuel", 27, "Masculino", 84.8f, 1.68f, true, 35);
        ClienteRegular cr2 = new ClienteRegular("Ana", 32, "Feminino", 65.2f, 1.65f, true, 20);
        ClienteEsporadico ce1 = new ClienteEsporadico("Joaquim", 25, "Masculino", 75.5f, 1.75f, true, 3);
        Convidado cv1 = new Convidado("José", 19, "Masculino", 60.5f, 1.60f, true, 2);
        Convidado cv2 = new Convidado("Tiago", 45, "Masculino", 87.5f, 1.72f, true, 5);

        Treinador t1 = new Treinador("Joana", 25, "Feminino", 625.15f);

        Funcionario f1 = new Funcionario("Diana", 38, "Feminino", 2);
        Funcionario f2 = new Funcionario("Ricardo", 40, "Masculino", 0);

        Pessoa utilizadoresGinasio[] = new Pessoa[8];
        utilizadoresGinasio[0] = cr1;
        utilizadoresGinasio[1] = cr2;
        utilizadoresGinasio[2] = ce1;
        utilizadoresGinasio[3] = cv1;
        utilizadoresGinasio[4] = cv2;
        utilizadoresGinasio[5] = t1;
        utilizadoresGinasio[6] = f1;
        utilizadoresGinasio[7] = f2;

        float receitas = 0.0f;
        float despesas = 0.0f;
        int clientesComMensalidade = 0;
        int funcionariosTreinadores = 0;
        for (int i = 0; i < utilizadoresGinasio.length; i++) {

            if (utilizadoresGinasio[i] instanceof Pagamento) {
                receitas += ((Pagamento) utilizadoresGinasio[i]).CalculaPagamento();
                clientesComMensalidade++;
                System.out.println("+" + ((Pagamento) utilizadoresGinasio[i]).CalculaPagamento() + " €");
            } else if (utilizadoresGinasio[i] instanceof Assalariado) {
                despesas += ((Assalariado) utilizadoresGinasio[i]).CalculaSalario();
                funcionariosTreinadores++;
                System.out.println("-" + ((Assalariado) utilizadoresGinasio[i]).CalculaSalario() + " €");
            }

        }
        System.out.println("Saldo do Ginásio=" + (double) Math.round((receitas - despesas) * 100) / 100 + " €");
        System.out.println("Clientes com mensalidade=" + clientesComMensalidade);
        System.out.println("Funcionarios+Treinadores=" + funcionariosTreinadores);

    }

}
