package tpi2_gym;

public class Convidado extends Clientes {

    private int Horas;

    public Convidado(String Nome, int idd, String genero, float peso, float altura, boolean activo, int Horas) {
        super(Nome, idd, genero, peso, altura, activo);
        this.Horas = Horas;
    }

    public int getHoras() {
        return Horas;
    }

    public void setHoras(int val) {
        this.Horas = val;
    }

    public void registarVisita(int horasVisitadas) {
        this.Horas = this.Horas - horasVisitadas;

    }

    @Override
    public String toString() {
        return super.toString()
                + "\nHoras restantes=" + Horas + "h";
    }

}
