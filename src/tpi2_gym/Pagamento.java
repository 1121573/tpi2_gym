package tpi2_gym;

public abstract class Pagamento extends Clientes {

    private float Valor;

    public Pagamento() {
        this.Valor = 0;
    }

    public Pagamento(String Nome, int idd, String genero, float peso, float altura, boolean activo, float Valor) {
        super(Nome, idd, genero, peso, altura, activo);
        this.Valor = Valor;
    }

    public float getValor() {
        return Valor;
    }

    public void setValor(float val) {
        this.Valor = val;
    }

    public abstract float CalculaPagamento();

}
