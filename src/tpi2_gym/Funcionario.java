package tpi2_gym;

public class Funcionario extends Assalariado {

    //variaveis de instancia
    private int clientesAngariados;

    //variaveis de class
    private static float SALARIO_BASE = 750.00f;
    private static float COMISSAO = 6.00f;

    //construtores
    public Funcionario(String Nome, int idd, String genero, int clientesAngariados) {

        super(Nome, idd, genero, SALARIO_BASE);
        this.clientesAngariados = clientesAngariados;
    }

    //metodos de instancia
    public int getClientesAngariados() {
        return clientesAngariados;
    }

    public void setClientesAngariados(int val) {
        this.clientesAngariados = val;
    }

    @Override
    public float CalculaSalario() {
        return (SALARIO_BASE + (COMISSAO * this.clientesAngariados));
    }

    @Override
    public String toString() {
        return super.toString() + "\nClientes Angariados=" + clientesAngariados
                + "\nVencimento= " + CalculaSalario() + "€";
    }

    //metodos de classe
    public static float getSALARIO_BASE() {
        return SALARIO_BASE;
    }

    public static void setSALARIO_BASE(float aSALARIO_BASE) {
        SALARIO_BASE = aSALARIO_BASE;
    }

    public static float getCOMISSAO() {
        return COMISSAO;
    }

    public static void setCOMISSAO(float aCOMISSAO) {
        COMISSAO = aCOMISSAO;
    }

}
