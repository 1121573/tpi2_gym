package tpi2_gym;

public class ClienteRegular extends Pagamento {

    public ClienteRegular(String Nome, int idd, String genero, float peso, float altura, boolean activo, float Valor) {
        super(Nome, idd, genero, peso, altura, activo, Valor);
    }

    @Override
    public float CalculaPagamento() {
        return this.getValor();
    }

    @Override
    public String toString() {
        return super.toString() + "\nMensalidade: " + CalculaPagamento() + "€";
    }

}
