package tpi2_gym;

public class ClienteEsporadico extends Pagamento {

    //variaceis de instancia
    private int horas;

    //variaveis de class
    private static float PRECO_HORA = 5.20f;

    public ClienteEsporadico() {
    }

    //construtor
    public ClienteEsporadico(String Nome, int idd, String genero, float peso, float altura, boolean activo, int horas) {
        super(Nome, idd, genero, peso, altura, activo, PRECO_HORA);
        this.horas = horas;
    }

    //metodos de instancia
    public int getHoras() {
        return horas;
    }

    public void setHoras(int val) {
        this.horas = val;
    }

    @Override
    public float CalculaPagamento() {

        return (float) Math.round((this.horas * PRECO_HORA) * 100) / 100;
    }

    @Override
    public String toString() {

        return super.toString() + "\nhoras=" + horas
                + "\nValor a Pagar:" + CalculaPagamento() + "€";
    }

    //metodos de classe
    public static float getPRECO_HORA() {
        return PRECO_HORA;
    }

    public static void setPRECO_HORA(float aPRECO_HORA) {
        PRECO_HORA = aPRECO_HORA;
    }

}
